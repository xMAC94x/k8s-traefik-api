# k8s-traefik-api

This crate only exports traefik ingress api created with kopium nothing more.

Software version:
k3s: `v1.31.5-k3s1`
kopium: `0.21.1`

It uses the integrated traefik from a k3s cluster which is `rancher/mirrored-library-traefik:2.11.18`

# Features
this crates depends on `k8s-openapi`, you are supposed to set its k8s version via a feature. That can be done directly in your Cargo.toml on the import of `k8s-openapi` or via the features on this crate.
