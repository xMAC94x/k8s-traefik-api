#/bin/bash
kopium ingressroutes.traefik.io -A -b --derive=Default --derive=PartialEq --smart-derive-elision > src/ingressroutes.rs
kopium ingressroutetcps.traefik.io -A -b --derive=Default --derive=PartialEq --smart-derive-elision > src/ingressroutetcps.rs
kopium ingressrouteudps.traefik.io -A -b --derive=Default --derive=PartialEq --smart-derive-elision > src/ingressrouteudps.rs
kopium middlewares.traefik.io -A -b --derive=Default --derive=PartialEq --smart-derive-elision > src/middlewares.rs
kopium middlewaretcps.traefik.io -A -b --derive=Default --derive=PartialEq --smart-derive-elision > src/middlewaretcps.rs
kopium serverstransports.traefik.io -A -b --derive=Default --derive=PartialEq --smart-derive-elision > src/serverstransports.rs
kopium serverstransporttcps.traefik.io -A -b --derive=Default --derive=PartialEq --smart-derive-elision > src/serverstransporttcps.rs
kopium tlsoptions.traefik.io -A -b --derive=Default --derive=PartialEq --smart-derive-elision > src/tlsoptions.rs
kopium tlsstores.traefik.io -A -b --derive=Default --derive=PartialEq --smart-derive-elision > src/tlsstores.rs
kopium traefikservices.traefik.io -A -b --derive=Default --derive=PartialEq --smart-derive-elision > src/traefikservices.rs
